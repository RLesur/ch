const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://rlesur.gitlab.io/ch/', {waitUntil: 'networkidle2'});
  await page.waitFor(() => !!document.querySelector('.footnote-area'));
  
  await page.pdf({path: 'hn.pdf', format: 'A4'});
  await browser.close();
})();