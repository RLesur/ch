# Largeurs

Entre trim marks, on a 37pc, soit 36pc de la taille de la page et 0.5pc pour les trim marks.

Les marges gauches et droit sont de 4pc auxquelles on ajoute les 0.5 pc pour les trim marks.


# Hauteurs

Entre trim marks, on estime à 55.7pc, soit plus que les 54pc.
Entre la trim mark du haut et le haut de la marge haute, on a environ 2.8pc.
Entre la trim mark du haut et le bas de la marge haute, on a environ 5.2pc.
Entre la trim du bas et le bas de la marge basse, on a environ 3.5pc.

Si on part de l'hypothèse que la marge d'impression est de 0.9pc.
En mettant une marge haute à 4pc, on a un truc très crédible.
En mettant également une marge basse à 4pc, ça devrait le faire à peu près.
